﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Threading.Tasks;
using System.Linq;

namespace SecureFlight.Core.Services
{
    public class ReservationService : IReservationService
    {
        private readonly IRepository<Passenger> _passengerRepository;
        private readonly IRepository<Flight> _flightRepository;
        public ReservationService(IRepository<Passenger> passengerRepository, IRepository<Flight>  flightRepository)
        {
            _passengerRepository = passengerRepository;
            _flightRepository = flightRepository;
        }

        public async Task Create(PassengerFlight passengerFlight)
        {
            var passenger = (await _passengerRepository.FilterAsync(p => p.Id == passengerFlight.PassengerId)).FirstOrDefault();
            if (passenger == null)
            {

            }

            var flight = (await _flightRepository.FilterAsync(f => f.Id == passengerFlight.FlightId)).FirstOrDefault();
            if (flight == null)
            {

            }

            passenger.PassengerFlights.Add(passengerFlight);
            _passengerRepository.Update(passenger);
        }

        public async Task Remove(PassengerFlight passengerFlight)
        {
            var passenger = (await _passengerRepository.FilterAsync(p => p.Id == passengerFlight.PassengerId)).FirstOrDefault();
            if (passenger == null)
            {

            }

            var flight = (await _flightRepository.FilterAsync(f => f.Id == passengerFlight.FlightId)).FirstOrDefault();
            if (flight == null)
            {

            }

            if (flight.Status.Id != Enums.FlightStatus.Active)
            {
                // The passenger can't cancel the reservation
            }

            passenger.PassengerFlights.RemoveAll(p => p.FlightId == passengerFlight.FlightId);
            _passengerRepository.Update(passenger);

            var passengerHasPendingFlights = passenger.Flights.Any(f => f.Id != passengerFlight.FlightId && f.Status.Id != Enums.FlightStatus.Cancelled && f.Status.Id != Enums.FlightStatus.Landed);
            if (passengerHasPendingFlights)
            {
                await _passengerRepository.Remove(passenger);
            }
        }
    }
}
