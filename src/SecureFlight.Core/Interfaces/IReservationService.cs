﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IReservationService
    {
        Task Create(PassengerFlight passengerFlight);
        Task Remove(PassengerFlight passengerFlight);
    }
}
