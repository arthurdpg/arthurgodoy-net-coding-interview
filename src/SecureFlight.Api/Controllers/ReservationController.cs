﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Interfaces;
using System.Collections.Generic;

namespace SecureFlight.Api.Controllers
{
    public class ReservationController : SecureFlightBaseController
    {
        private readonly IReservationService _reservationService;

        public ReservationController(IMapper mapper, IReservationService reservationService) : base(mapper)
        {
            _reservationService = reservationService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public IActionResult Post(ReservationDataTransferObject reservation)
        {
            return null;
        }
    }
}
