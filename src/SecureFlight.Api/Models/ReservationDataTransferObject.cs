﻿namespace SecureFlight.Api.Models
{
    public class ReservationDataTransferObject
    {
        public string PassengerId { get; set; }
        public long FlightId { get; set; }
    }
}
